/*  Projekt c.2
 *
 *  Matej Caja
 *  PPLS, 7.skupina
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define SUBOR "AUTA.txt"
#define MAX 52

//  zadefinovanie struktury
typedef struct auta{
    char znak[3];
    char kategoria[MAX];
    char znacka[MAX];
    char predajca[102];
    int cena;
    int rok_vyroby;
    char stav_vozidla[202];
    struct auta *dalsi;
}AUTA;

/* uplny funkcny prototyp vsetkych funkcii pouzitych v projekte */
FILE *otvor(FILE **fr);
FILE *zatvor(FILE **fr);
AUTA *alokuj();
AUTA *nacitaj(AUTA *prvy, FILE *fr);
AUTA *vypis(AUTA *prvy);
AUTA *pridaj(AUTA *prvy);
AUTA *zmaz(AUTA *prvy);
AUTA *vyhladaj(AUTA *prvy);
AUTA *aktualizuj(AUTA *prvy);
AUTA *ukonc(AUTA *prvy);

/*  Main cita znaky z klavesnice a pomocou switch-u spusta funkciu ku danemu nacitanemu znaku, z cyklu sa vyskakuje pomocou
    navestia goto;
*/
int main()
{
    FILE *fr;
    AUTA *prvy = NULL;
    int c;

    while(1){
        c = getchar();
        switch(c){
            case 'N' :  fr = otvor(&fr);
                        prvy = nacitaj(prvy, fr);
                        fr = zatvor(&fr);
            break;

            case 'V' :  prvy = vypis(prvy);
            break;

            case 'P' :  prvy = pridaj(prvy);
            break;

            case 'Z' :  prvy = zmaz(prvy);
            break;

            case 'H' :  prvy = vyhladaj(prvy);
            break;

            case 'A' :  prvy = aktualizuj(prvy);
            break;

            case 'K' :  prvy = ukonc(prvy);
                        goto koniec;
            break;
        }
    }

    koniec:

    return 0;
}

//  funkcia na otvorenie suboru
FILE *otvor(FILE **fr)
{
    if((*fr = fopen(SUBOR, "r")) == NULL){
        printf("Zaznamy  neboli  nacitane\n");
        return NULL;
        }

    return *fr;
}

//  funkcia na zatvorenie suboru
FILE *zatvor(FILE **fr)
{
    if(fclose(*fr) == EOF){
        printf("Neuzavrety subor\n");
        return NULL;
    }

    return *fr;
}

// funkcia na alokovanie pamate, vyuziva sa na pridelenie pamate jednotlivým premenným typu AUTA
AUTA *alokuj()
{
    AUTA *alokuj;

    if((alokuj = (AUTA *) malloc(sizeof(AUTA))) == NULL){
        printf("Nedostatok pamate\n");
        exit(1);
    }

    return alokuj;
}

/*  funkcia(v pripade opatovneho stlacenia N vyprazdni zaznamy a nacita ich nanovo zo suboru) prejde suborom,
    zisti pocet jednotlivych zaznamov, nacita prvy zaznam a potom v cykle for nacitava zvysne zaznamy
    zo suboru, na konci sa ukazuje na NULL
*/
AUTA *nacitaj(AUTA *prvy, FILE *fr)
{
    AUTA *pom;
    int c, counter = 0;
    int n = 1;
    int i;

    if(prvy != NULL){
        for(pom = prvy; pom != NULL; ){
            prvy = pom;
            pom = pom -> dalsi;
            free((void *)prvy);
        }
	}

    while((c = getc(fr)) != EOF){
        if(c  == '$')
            counter++;
    }

    rewind(fr);
    prvy = alokuj();

    fscanf(fr, "%s", prvy->znak);
    fgetc(fr);

    fgets(prvy->kategoria, MAX, fr);
    prvy->kategoria[strlen(prvy->kategoria) - 1] = '\0';

    fgets(prvy->znacka, MAX, fr);
    prvy->znacka[strlen(prvy->znacka) - 1] = '\0';

    fgets(prvy->predajca, MAX, fr);
    prvy->predajca[strlen(prvy->predajca) - 1] = '\0';

    fscanf(fr, "%d", &prvy->cena);
    fgetc(fr);

    fscanf(fr, "%d", &prvy->rok_vyroby);
    fgetc(fr);

    fgets(prvy->stav_vozidla, MAX, fr);
    prvy->stav_vozidla[strlen(prvy->stav_vozidla) - 1] = '\0';

    prvy->dalsi = NULL;
    pom = prvy;

    for(i = 1; i < counter; i++){
        pom->dalsi = alokuj();
        pom = pom->dalsi;

        fscanf(fr, "%s", pom->znak);
        fgetc(fr);

        fgets(pom->kategoria, MAX, fr);
        pom->kategoria[strlen(pom->kategoria) - 1] = '\0';

        fgets(pom->znacka, MAX, fr);
        pom->znacka[strlen(pom->znacka) - 1] = '\0';

        fgets(pom->predajca, MAX, fr);
        pom->predajca[strlen(pom->predajca) - 1] = '\0';

        fscanf(fr, "%d", &pom->cena);
        fgetc(fr);

        fscanf(fr, "%d", &pom->rok_vyroby);
        fgetc(fr);

        fgets(pom->stav_vozidla, MAX, fr);
        pom->stav_vozidla[strlen(pom->stav_vozidla) - 1] = '\0';
        n++;
    }

    pom->dalsi = NULL;
    printf("Nacitalo sa %d zaznamov\n", n);

    return prvy;
}
// funkcia vypise jednotlive zaznamy struktury
AUTA *vypis(AUTA *prvy)
{
    AUTA *pom;
    int i = 1;

    if(prvy == NULL){
        return NULL;
    }

    pom = prvy;

    while(pom != NULL){
        printf("%d.\n", i);
        printf("kategoria: %s\n", pom->kategoria);
        printf("znacka: %s\n", pom->znacka);
        printf("predajca: %s\n", pom->predajca);
        printf("cena: %d\n", pom->cena);
        printf("rok_vyroby: %d\n", pom->rok_vyroby);
        printf("stav_vozidla: %s\n", pom->stav_vozidla);
        i++;
        pom = pom->dalsi;
    }

    return prvy;
}
//  funkcia porovnava nacitanu sumu od uzivatela so sumou v zaznamoch,
//  vypise vsetky zanzamy ktore su rovne alebo mensie od nacitanej sumy
AUTA *vyhladaj(AUTA *prvy)
{
    AUTA *pom;
    int c;
    int n = 1;
    int counter = 0;

    pom = prvy;

    scanf("%d", &c);

    while(pom != NULL){
        if((pom->cena) <= c){
            printf("%d.\n", n);
            printf("kategoria: %s\n", pom->kategoria);
            printf("znacka: %s\n", pom->znacka);
            printf("predajca: %s\n", pom->predajca);
            printf("cena: %d\n", pom->cena);
            printf("rok_vyroby: %d\n", pom->rok_vyroby);
            printf("stav_vozidla: %s\n", pom->stav_vozidla);
            n++;
            counter++;
        }
        pom = pom->dalsi;
    }

    if(counter == 0){
        printf("V ponuke su len auta s vyssou cenou\n");
    }

    return prvy;
}
//  funkcia nacita miesto a novy zaznam od uzivatela, a na zaklade nacitaneho cisla vlozi zaznam medzi zvysne zaznamy
AUTA *pridaj(AUTA *prvy)
{
    AUTA *pom, *akt, *novy;
    int p;
    int c;
    int i = 1;

    pom = prvy;
    akt = prvy;
    novy = alokuj();

    scanf("%d", &p);

    c = getchar();

    fgets(novy->kategoria, MAX, stdin);
    novy->kategoria[strlen(novy->kategoria) - 1] = '\0';

    fgets(novy->znacka, MAX, stdin);
    novy->znacka[strlen(novy->znacka) - 1] = '\0';

    fgets(novy->predajca, MAX, stdin);
    novy->predajca[strlen(novy->predajca) - 1] = '\0';

    scanf("%d", &novy->cena);
    c = getchar();

    scanf("%d", &novy->rok_vyroby);
    c = getchar();

    fgets(novy->stav_vozidla, MAX, stdin);
    novy->stav_vozidla[strlen(novy->stav_vozidla) - 1] = '\0';


    if(prvy == NULL){           //  ak nieje v strukture ziaden zaznam, prida sa novy zaznam na zaciatok
        novy->dalsi = NULL;
        prvy = novy;
    }
    else if(p == 1){            // ak sa ma pridat zaznam na zaciatok, novy sa priradi na miesto prveho a tam kam ukazuje dalsi prvok od noveho
        novy->dalsi = prvy;     // sa priradi prvy
        prvy = novy;
    }
    else{
        while(akt != NULL){
            if(p == i){                 //  ak sa ma pridat medzi zaznamy, posunie sa akt na dalsi a pom(predchadzajuci) ukazuje na novy
                novy->dalsi = akt;
                pom->dalsi = novy;
            }
            pom = akt;
            akt = akt->dalsi;
            i++;
        }
        if(p >=  i){                    //  ak sa ma zaradit na koniec, novy ukazuje na NULL(koniec zaznamu) a pom(predchadzajuci) ukazuje na novy
        novy->dalsi = NULL;
        pom->dalsi = novy;
        }
    }


    return prvy;
}
// funkcia zmaze zaznamy, ktore sa zhoduju s nacitanym retazcom
AUTA *zmaz(AUTA *prvy)
{
    AUTA *pom, *pred, *akt, *zmaz, *novy;
    char znacka[MAX];
    int c;
    int n = 0;
    int i;

    zmaz = alokuj();
    novy = alokuj();
    akt = prvy;
	pred = prvy;

    c = getchar();
    fgets(znacka, MAX, stdin);
    znacka[strlen(znacka) - 1] = '\0';

    if(strcmpi(akt->znacka, znacka) == 0){      //  mazanie pre prvy zaznam v strukture
        novy = prvy;
        prvy = NULL;
        free(prvy);
        prvy = novy->dalsi;
        n++;
    }

        akt = prvy;

        while(akt != NULL){                 //  mazanie pre zvysne zaznamy
            pred = akt;
            pom = akt->dalsi;
            while(pom != NULL){
                if(strcmpi(pom->znacka, znacka) == 0){
                    zmaz = pom;                     //  ak sa zhoduju, do zmaz sa priradi premenna ktora sa zmaze
                    pred->dalsi = pom->dalsi;       //  pred ukazujuca na prvok ktory sa ma zmazat, ukazuje na zaznam za zaznamom,
                    pom = pom->dalsi;               //  ktory sa ma zmazat, a vymaze sa nechceny zaznam
                    zmaz = NULL;
                    free(zmaz);
                    n++;
                }
                else{
                    pred = pom;
                    pom = pom->dalsi;
                }
            }
            akt = akt->dalsi;
       }

    printf("Zmazalo sa %d zaznamov\n", n);

    return prvy;
}
/*  funkcia aktualizuje vsetky zaznamy, ktore sa zhoduju s nacitanym retazcom, vyuziva sa funkcia strncmp
    a vsetky znaky v danych porovnavanych retazcoch sa nastavia na velke
*/
AUTA *aktualizuj(AUTA *prvy)
{
    AUTA *pom, *akt;
    int n = 0;
    int i;
    int c;
    char predajca2[102];
    char znacka2[MAX];
    int cena2;
    int rok_vyroby2;
    char stav_vozidla2[202];
    char novy[MAX];
    char kategoria2[MAX];

    pom = prvy;
    akt = prvy;

    c = getchar();
    fgets(novy, MAX, stdin);
    novy[strlen(novy) - 1] = '\0';

    fgets(kategoria2, MAX, stdin);
    kategoria2[strlen(kategoria2) - 1] = '\0';

    fgets(znacka2, MAX, stdin);
    znacka2[strlen(znacka2) - 1] = '\0';

    fgets(predajca2, 102, stdin);
    predajca2[strlen(predajca2) - 1] = '\0';

    scanf("%d", &cena2);
    c = getchar();

    scanf("%d", &rok_vyroby2);
    c = getchar();

    fgets(stav_vozidla2, 202, stdin);
    stav_vozidla2[strlen(stav_vozidla2) - 1] = '\0';

    for(i = 0; i < strlen(novy); i++){  //zvysujem vsetky chary retazca na velke
        novy[i] = toupper(novy[i]);
    }

    while(pom != NULL && akt != NULL){
        for(i = 0; i < strlen(akt->znacka); i++)    //zvysujem vsetky chary retazca na velke
            akt->znacka[i] = toupper(akt->znacka[i]);

        if(strncmp(novy, akt->znacka, 2) == 0){     //vyuzivam funkciu strncmp ak by bola zadana len cast znacky auta, preto kontrolujem len 2 znaky
            strcpy(pom->kategoria,kategoria2);
            strcpy(pom->znacka, znacka2);
            strcpy(pom->predajca, predajca2);
            pom->cena = cena2;
            pom->rok_vyroby = rok_vyroby2;
            strcpy(pom->stav_vozidla, stav_vozidla2);

            n++;
        }
        pom = pom->dalsi;
        akt = akt->dalsi;
    }

    printf("Aktualizovalo sa %d zaznamov\n", n);

    return prvy;
}
/*  funkcia uvolni pamat a skonci program */
AUTA *ukonc(AUTA *prvy)
{
    AUTA *pom;
    pom = prvy;

    for(pom = prvy; pom != NULL; ){
		prvy = pom;
        pom = pom -> dalsi;
        free((void *)prvy);
    }

    return 0;
}
